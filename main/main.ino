#include <BTS7960.h>

#include <buffer.h>
#include <crc.h>
#include <datatypes.h>
#include <VescUart.h>

// Yaw Position Motor - Left/right
#define YAW_EN 0
#define YAW_L_PWM 1
#define YAW_R_PWM 2

// Pitch Position Motor - up/down
#define PITCH_EN 3
#define PITCH_L_PWM 4
#define PITCH_R_PWM 5

// Windshield Motor - Push puck to launch
#define WIN_MOTOR_PIN 6

// Flipsky ESC Setup - Shooting speed
VescUart VESCUART;

const uint8_t PotentiometerPin = A0;

uint32_t LastTime = 0; 

 // Yaw Motor
  BTS7960 yaw_motor(YAW_EN, YAW_L_PWM, YAW_R_PWM);

  // Pitch Motor 
  BTS7960 pitch_motor(PITCH_EN , PITCH_L_PWM, PITCH_L_PWM);

// Function prototypes
void linearFunction(int, char, BTS7960);

void setup() {

  // Flipsky ESC
  Serial.begin(115200);
  
  while (!Serial1) {;}

  VESCUART.setSerialPort(&Serial1);

  // Windshield Motor
  pinMode(WIN_MOTOR_PIN, OUTPUT); 

}

void loop() {

  if((millis() - LastTime) > 10){
    // Speed
    VESCUART.nunchuck.valueY = map(analogRead(PotentiometerPin), 0, 1023, 0, 255);

    VESCUART.setNunchuckValues();

    LastTime = millis();


   // TODO: 3. COMPLETE BELOW FUNCTIONS FIRST. Add some example control commands. For example move machien left, right, up down then shoot. Use the functions from bellow. This shouldn't call the methods for the motor directly. 

  linearFunction(20, 'L', yaw_motor);
  linearFunction(40, 'R', yaw_motor);
  linearFunction(20, 'L', yaw_motor);

  linearFunction(20, 'L', pitch_motor);
  linearFunction(40, 'R', pitch_motor);
  linearFunction(20, 'L', pitch_motor);

  toggleWindshield();
 }
 }
 // TODO: 1. This link has the guide https://github.com/luisllamasbinaburo/Arduino-BTS7960 for the motor controls.  
 // We should have a function that maps the pwm output to a number of mm moved.
 // So the function accepts a distance in mm and direction and the specific motor and then writes the appropriate PWM value and direction.

unsigned long pwm_output_time;
const int linear_speed = 30; // 30 [mm/s] for linear actuators

void linearFunction(int dist,char dir, BTS7960 motor){  // dist = distance in millimeters, dir = left (L) or right (R), up (U) or down (D)
  
  int time_required = 1000 * (linear_speed / dist); // Time in milliseconds required for the motor to move
  motor.Enable();
  
  if(dir == 'L'){
    pwm_output_time = millis();
    while((millis()-pwm_output_time) <= (time_required)){
      motor.TurnLeft(255);  // Real directions are still unclear, need to be tested with the prototype? 255 should be the max. speed?
      }
    //motor.Disable();
  }
    
  if(dir == 'R'){
    pwm_output_time = millis();
    while((millis()-pwm_output_time) <= (time_required)){
      motor.TurnRight(255);
      }
    //motor.Disable();
  }
  
} // end of linearFunction



 // TODO: 2. This function should enable/disable the windshield wiper motor for one full cycle. So it writes the pin high for enough time then writes it low. This function should not be blocking. 

const unsigned long pushing_cycle = 1000;  // 1000 milliseconds is now the cycle of pushing one puck 
unsigned long push_the_puck;               //variable for the Windshield motor

void toggleWindshield(){

    if (digitalRead (WIN_MOTOR_PIN) == LOW){
      digitalWrite (WIN_MOTOR_PIN, HIGH);
      push_the_puck = millis();
    }
    
    if((millis()-push_the_puck) >= (pushing_cycle)){
      digitalWrite (WIN_MOTOR_PIN, LOW);
    }
  } // end of toggleWindshield


  
