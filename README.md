# slapshot-core

Core controller code for the slapshot hockey puck shooter

Use the arduino IDE from the windows store for development

Boards Manager:
* Arduino SAMD Boards (32-bit ARM Corex-M0 +) - 1.8.11

Packages: 

* Requires installation from source of [VescUart Library](https://github.com/SolidGeek/VescUart)
* Requires installation from pacakage manager [BTS7960 Library](https://github.com/luisllamasbinaburo/Arduino-BTS7960)

